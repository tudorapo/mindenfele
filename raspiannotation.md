# Parts which are not well documented

The `raspistill` utility is more or less a direct interface to the camera of a
Raspberry Pi. 
The Raspberry Pi started it's glorious life as a mobile phone platform,
therefore it is using non-standard or not-really-known variables to set up
things.

One of these is the annotation system. The tool has a quite flexible 
annotation feature built in, but it's not too well documented. Here it is all 
we have on it:
```bash
$ raspistill | grep ann
-a, --annotate  : Enable/Set annotate flags or text
-ae, --annotateex       : Set extra annotation parameters (text size, text colour(hex YUV), bg colour(hex YUV), justify, x, y)
```
Which is a bit too laconic. Also it's a lie. This is how it looks like after
a hour of digging up code:
```bash
raspistill -w 640 -h 480 -q 95 -a 1029 -a "Streetcam V0.1 %F %H:%M:%S" -ae 15,0000ff,808000,2,6,440
```
This will create a picture with a white annotation on a black rectangle in
the lower right corner of the picture, it's height will be 15 pixels, and it
will show the date and time next to the text.

But why, you may ask. 

## Annotate

To make life easier, the `-a` option does two things. 
It can be a bitmap or a string. 

If it's a bitmap, it writes a bunch of photo parameters to the image, if it's
a string it's written to the image.
I'm using `ANNOTATE_USER_TEXT+ANNOTATE_DATE_TEXT+ANNOTATE_BLACK_BACKGROUND`, 
which is 1029, and allows me to use strftime like time specifications.

The second -a is the string itself, so this part looks like this:
```bash
-a 1029 -a "Streetcam V0.1 %F %H:%M:%S"
```

## Extended features

To make life easier, the documentation of the `-ae` option contains only one
lie (twice). 

The text size appears to be in pixels, very conveniently. The code says that 
it's valid between 6 and 80.

The colorspace is hex VUY, so if you know the color's hexadecimal YUV code you
just have to turn them around. Easypeasy. The background color is only visible 
if the `ANNOTATE_BLACK_BACKGROUND` bit is added to the first `-a` bitmap.

Justify is simple:

| Number | Result |
| ------:|:------ |
| 0 | Center |
| 1 | Left |
| 2 | Right |

Only horizontal justify is set, the vertical one has to be set by the 
remaining two options. 

The offsets are also in pixel, and used in a helpful manner. Using right
justify and 20 as the horizontal offset, the right end of the annotation will
be 20 pixel away from the right edge of the picture. 

## Sources

What the damn options do:

https://github.com/raspberrypi/userland/blob/master/host_applications/linux/apps/raspicam/RaspiCamControl.c#L728

Predefined annotation option bits:

https://github.com/raspberrypi/userland/blob/master/host_applications/linux/apps/raspicam/RaspiCamControl.h#L94

How the YUV colorspace looks like in hex:

http://stnsoft.com/Muxman/Usage/Palettes/color_syntax.shtml
