#!/bin/dash

. ./config

getbat() {
    cat /sys/class/power_supply/BAT0/capacity
}

export DISPLAY=:0.0

OLDBAT=$(getbat)

while true; do
    NEWBAT=$(getbat)
    if [ "$NEWBAT" -lt "$OLDBAT" ] ; then
        logger "Losing power, $NEWBAT vs $OLDBAT !";
        OLDBAT=$NEWBAT;
	COMMAND="stay"
        if [ "$OLDBAT" -lt "$ALARM" ] ; then
            xmessage -buttons stay:10,sleep:11 -default sleep -nearmouse -timeout "$ALARMTIME" "Battery is at $OLDBAT %."
	    RETVAL=$?
  	    if [ "$RETVAL" -eq "0" -o "$RETVAL" -eq "11" ] ; then COMMAND="sleep"; fi
        elif [ "$OLDBAT" -lt "$WARN" ] ; then
            xmessage -buttons stay:10,sleep:11 -default stay -nearmouse -timeout "$WARNTIME" "Battery is at $OLDBAT %."
	    RETVAL=$?
  	    if [ "$RETVAL" -eq "11" ] ; then COMMAND="sleep"; fi
        fi
        if [ "$COMMAND" = "sleep" ] ; then
	    xmessage "Going to sleep because of battery." &
	    sleep 5
            logger "Going to sleep now!"
	    echo mem > /sys/power/state
        fi
    fi
    sleep 5;
done
