package main

import (
    "fmt"
    "os"
    "flag"
    "github.com/vharitonsky/iniflags"
    "github.com/sqweek/dialog"
    "time"
    "log"
    "log/syslog"
)
var alarm = flag.Int("alarm", 5, "Percentage of battery charge when the system goes to sleep without user interaction.")
var warn  = flag.Int("warn", 10, "Percentage of battery charge when the annoying popups start.") 
var alarmto = flag.Int("alarmto", 20, "Seconds to wait for the user to click on the shutdown popup.")
var warnto  = flag.Int("warnto",   5, "Seconds to wait for the user to click on the warning popup.")

func getbat() (batt int) {
    f,_ := os.Open("/sys/class/power_supply/BAT0/capacity")
    fmt.Fscanf(f, "%d\n", &batt)
    f.Close()
    return
}

func popup(title string, timeout int, batt int) (cmd bool, tou bool) {
    command := make(chan bool) 
    go func() {
        command <- dialog.Message("We have only %d%% in the battery!\nShould we go to sleep?", batt).Title(title, timeout).YesNo()
    }()
    select {
        case cmd = <-command:
        case <- time.After(time.Duration(timeout) * time.Second):
            tou = true
        }
    return
    }

func main() {
    var newbat, oldbat int
    var cmd, tou bool

    iniflags.SetConfigFile("config")
    iniflags.Parse()
    logwriter, _ := syslog.New(syslog.LOG_NOTICE, "batmon")
    log.SetOutput(logwriter)

    oldbat = 100
    for {
        newbat = getbat()
        if newbat < oldbat {
            log.Printf("Battery going from %d to %d.", oldbat, newbat)
            oldbat = newbat
            if newbat < *alarm {
                cmd, tou = popup("ALARM!", *alarmto, newbat)
                cmd = cmd || tou
            } else  if newbat < *warn {
                cmd, tou = popup("Warning?", *warnto, newbat)
            }
            if cmd {
                log.Print("Going to sleep")
                f,_ := os.Create("state")
                f.WriteString("mem\n")
                f.Close()
            }
        }
        <- time.After(5 * time.Second)
    }
}
