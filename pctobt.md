# Using a linux computer as a bluetooth speaker

Let's say you have a pc with a nice set of speakers and a tiny little phone.
How to convert your pc to a bt speaker?

## Install stuff
```
apt install bluetooth bluez-tools ofono

apt install bluez-alsa
#If it does not exists, as it does not exists in debian

apt install libfdk-aac-dev libasound2-dev bluez-tools libbluetooth-dev \
   libsbc-dev libmp3lame-dev libfaac-dev libdbus-1-dev libbsd-dev \
    libncurses-dev libglib2.0-dev
git clone https://github.com/Arkq/bluez-alsa.git
cd bluez-alsa
autoreconf --install
mkdir build 
cd build
../configure --enable-aac --enable-ofono --enable-debug --enable-mp3lame --enable-hcitop
make
make install
```

## Configuring stuff

In the file `/etc/bluetooth/main.conf`:
1. Change the device name to your liking
1. I changed the `DiscoverableTimeout` to `0` so the device is always discoverable. 
    It has power so why not?

After this restart the `bluetooth` services.

## Pairing

Use `bluetoothctl` to scan for new devices:

```
[bluetooth]# scan on
Discovery started
[CHG] Controller 04:D3:B0:86:10:2C Discovering: yes
[NEW] Device DE:AD:BE:EF:MA:MA Bogyo                    ====> Mine!
[NEW] Device 5F:25:46:5E:D5:A3 5F-25-46-5E-D5-A3
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 00001105-0000-1000-8000-00805f9b34fb
[lot of stuff]
[bluetooth]# scan off
[CHG] Device E4:7D:BD:D3:05:E5 TxPower is nil
[CHG] Device E4:7D:BD:D3:05:E5 RSSI is nil
[CHG] Device 5F:25:46:5E:D5:A3 TxPower is nil
[CHG] Device 5F:25:46:5E:D5:A3 RSSI is nil
[CHG] Device DE:AD:BE:EF:MA:MA RSSI is nil
[CHG] Controller 04:D3:B0:86:10:2C Discovering: no
Discovery stopped

```

As you see plenty of bluetooth devices are around here. 
With some luck one can find their own phone, mine shows the mac on the 
screen, but with some luck it's name is shown on.

For pairing the pc and the phone both checks the magic number:

```
bluetooth]# pair DE:AD:BE:EF:MA:MA
Attempting to pair with DE:AD:BE:EF:MA:MA
[CHG] Device DE:AD:BE:EF:MA:MA Connected: yes
Request confirmation
[agent] Confirm passkey 935366 (yes/no): yes
[CHG] Device DE:AD:BE:EF:MA:MA Modalias: bluetooth:v003Cp00C1d0810
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 00001105-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 0000110a-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 0000110c-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 0000110e-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 00001112-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 00001115-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 00001116-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 0000111f-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 0000112d-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 0000112f-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 00001132-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 00001200-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 00001800-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA UUIDs: 00001801-0000-1000-8000-00805f9b34fb
[CHG] Device DE:AD:BE:EF:MA:MA ServicesResolved: yes
[CHG] Device DE:AD:BE:EF:MA:MA Paired: yes
Pairing successful
[CHG] Device DE:AD:BE:EF:MA:MA ServicesResolved: no
[CHG] Device DE:AD:BE:EF:MA:MA Connected: no
```

And now that they are friends, we can query what we have:

```
[bluetooth]# info DE:AD:BE:EF:MA:MA
Device DE:AD:BE:EF:MA:MA (public)
        Name: Bogyo
        Alias: Bogyo
        Class: 0x005a020c
        Icon: phone
        Paired: yes
        Trusted: no
        Blocked: no
        Connected: no
        LegacyPairing: no
        UUID: OBEX Object Push          (00001105-0000-1000-8000-00805f9b34fb)
        UUID: Audio Source              (0000110a-0000-1000-8000-00805f9b34fb)
        UUID: A/V Remote Control Target (0000110c-0000-1000-8000-00805f9b34fb)
        UUID: A/V Remote Control        (0000110e-0000-1000-8000-00805f9b34fb)
        UUID: Headset AG                (00001112-0000-1000-8000-00805f9b34fb)
        UUID: PANU                      (00001115-0000-1000-8000-00805f9b34fb)
        UUID: NAP                       (00001116-0000-1000-8000-00805f9b34fb)
        UUID: Handsfree Audio Gateway   (0000111f-0000-1000-8000-00805f9b34fb)
        UUID: SIM Access                (0000112d-0000-1000-8000-00805f9b34fb)
        UUID: Phonebook Access Server   (0000112f-0000-1000-8000-00805f9b34fb)
        UUID: Message Access Server     (00001132-0000-1000-8000-00805f9b34fb)
        UUID: PnP Information           (00001200-0000-1000-8000-00805f9b34fb)
        UUID: Generic Access Profile    (00001800-0000-1000-8000-00805f9b34fb)
        UUID: Generic Attribute Profile (00001801-0000-1000-8000-00805f9b34fb)
        Modalias: bluetooth:v003Cp00C1d0810
```

As a phone it can do a lot of things. But we can not connect yet!

```
[bluetooth]# connect DE:AD:BE:EF:MA:MA
Attempting to connect to DE:AD:BE:EF:MA:MA
Failed to connect: org.bluez.Error.Failed
```

The system log says:

```
a2dp-source profile connect failed for DE:AD:BE:EF:MA:MA: Protocol not available
```

We have to provide a way for the sound to be registered, this is why we have 
`bluez-alsa`. It can be done by pulseaudio too, but that is from the Devil 
and as such it was abolished from these premises.

## Making sound

Start the magic with `bluealsa -p a2dp-source -p a2dp-sink -p hfp-ofono`.

I'm not entirely sure which of the above is necessary, because the general 
shakiness of the bluetooth stack caused me to kill the bt in my server. 

Anyway! After 'rmmod btusb' and 'modprobe btusb', and using `bluetoothctl`
again:


```
[bluetooth]# trust DE:AD:BE:EF:MA:MA
[CHG] Device DE:AD:BE:EF:MA:MA Trusted: yes
Changing DE:AD:BE:EF:MA:MA trust succeeded
[bluetooth]# connect DE:AD:BE:EF:MA:MA
Attempting to connect to DE:AD:BE:EF:MA:MA
[CHG] Device DE:AD:BE:EF:MA:MA Connected: yes
Connection successful
[CHG] Device DE:AD:BE:EF:MA:MA ServicesResolved: yes
[CHG] Device E4:7D:BD:D3:05:E5 RSSI: -94
```

Exuberance! If we start a music player on the phone it will be quiet. 
This is because the sound arrives, but nothing pushes it to the sound card.
We need another tool from `bluez-alsa`:

`bluealsa-aplay -d "default:CARD=DX" DE:AD:BE:EF:MA:MA`

I have to provide the `-d` option because I have a sound card for not
entirely clear reasons. 

At this point music should come out of the speakers.

## Unresolved issues

1. There is something silly about connecting the phone. 
    It does not works all the time. Annoying
1. Does it reconnects automatically? Good question. 
1. How can I start a new `bluealsa-aplay` instance for every connected 
    device? Systemd and/or dbus could do it, I'm pretty sure. Fortunately
    I need this for one test. I have audi cables and media player on the
    server. 
1. How could I use names instead of macs?
