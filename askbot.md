# Askbot, ngnix, uwsgi

A short description of how I installed askbot under my existing uwsgi/nginx setup.

## Steps

1. `mkdir /opt/csaszar/askbot0`
1. `chown tudor:tudor /opt/csaszar/askbot0`
1. `cd /opt/csaszar/askbot0/`
1. `virtualenv venv`
1. `. venv/bin/activate`
1. `pip install askbot`
1. `pip install psycopg2-binary`
1. `pip install six==1.10.0`
1. `pip install python-memcached` _(I have a memcached and the thing complained at startup)_
1. Configure a database.
1. `askbot-setup` _(Install it to the directory askbottest. Or any other dir, we need it for the configuration to have it in a subdir inside askbot0)_
1. Edit `settings.py` and set up the host of the database, as it's not done by `askbot-setup`.
1. `python manage.py syncdb`
1. `python manage collectstatic`
1. Create an application ini for uwsgi and add the new directories to the ngnix config, see below.
1. Enjoy!

## Configuration files

### Uwsgi app.ini

I'm not sure if every parameters are used. For example the static map can be handled differently by ngnix config, the wsgi-file option is actually missing a `/` so it's surely not used.


```
[uwsgi]
socket = /tmp/askbot0.sock
chmod-socket = 666
chown-socket = tudor:tudor
uid = tudor
gid = tudor 
chdir = /opt/csaszar/askbot0/
home = /opt/csaszar/askbot0/venv/
logto = /opt/csaszar/askbot0/askbottest/log/log.log
pidfile =  /tmp/askbot0.pid
wsgi-file = /opt/csaszar/askbot0/askbottestdjango.wsgi
static-map = /m=/opt/csaszar/askbot0/askbottest/static/
env = DJANGO_SETTINGS_MODULE=askbottest.settings
module = django.core.wsgi:get_wsgi_application()
threads = 1
workers = 4
master = true
max-requests = 1000
harakiri = 12
buffer-size = 24576
```

### Nginx snippet

```
server {
	listen 80 default_server;
	listen [::]:80 default_server;
	location /guyhasquestions/ { #@yourapplication {
		include uwsgi_params;
		uwsgi_pass unix:/tmp/askbot0.sock;
	}
	location /guyhasquestions/m/{
          alias /opt/csaszar/askbot0/askbottest/static/;
        }
	location /guyhasquestions/upfiles/{
          alias  /opt/csaszar/askbot0/askbottest/askbot/upfiles/;
        }

}
```

### Django config

Which is actually the `settings.py`, only the parts I changed.

This is the part where the host must be added manually.

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'askbot0',                      # Or path to database file if using sqlite3.
        'USER': 'askbot',                      # Not used with sqlite3.
        'PASSWORD': 'kerdezz',                  # Not used with sqlite3.
        'HOST': 'psql',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
        'TEST_CHARSET': 'utf8',              # Setting the character set and collation to utf-8
        'TEST_COLLATION': 'utf8_general_ci', # is necessary for MySQL tests to work properly.
    }
}
```
This is where we can set up to have separate `/m` ad `/upfiles` directories to separate multiple askbot instances.
```
# Absolute path to the directory that holds uploaded media
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'askbot', 'upfiles')
MEDIA_URL = '/guyhasquestions/upfiles/'
STATIC_URL = '/guyhasquestions/m/'#this must be different from MEDIA_URL
```
Memcached for the memcachedly gifted.
```
#setup memcached for production use!
# See http://docs.djangoproject.com/en/1.8/topics/cache/ for details.
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': None,
        # Chose a unique KEY_PREFIX to avoid clashes with other applications
        # using the same cache (e.g. a shared memcache instance).
        'KEY_PREFIX': 'askbottest',
    }
}
```
Again for separate installs.
```
###########################
#
#   this will allow running your forum with url like http://site.com/forum
#
#   ASKBOT_URL = 'forum/'
#
ASKBOT_URL = 'guyhasquestions/' #no leading slash, default = '' empty string
```

Thats all.
