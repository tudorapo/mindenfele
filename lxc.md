# Foreword

I am mostly at home, confined here by an aching knee and some mandatory vacation 
days. To help pass the time, i watched movies and rebuilt my home server
to amd64 architecture. 

As a part of this process, i planned to put some services to virtual machines,
to make the host system simpler. After asking around i found that everyone is 
ing various virtualisation and contanerization tools, but no one is
using [LXC](https://linuxcontainers.org/). Now there is someone.

# What is LXC?

LXC is supposed to lie between a chroot and a 'real' virtual machine. A lot
of work was done to separate the inside world from the outside world, but
this means some restrictions. Especially the various `/sys` style modern tools
has to be handled carefully, because the container and the host are using the
same kernel, and setting the kernel variables of the host from the container
is a Bad Thing (tm). 

Cgroups, devfs, syfs all has some limitations and plenty of tricks i have not 
explored, but the only thing which was not working was systemd. Which is not
too important, except that it is supposed to start everything else. But there
is a workaround.

Those problems do not exist in a KVM/Xen/VmWare/Etc virtual machine, but those
will use memory for at least another kernel, waste cpu cycles to imitate
disk drivers, network cards, video cards etc. Cheaper is always crappier,
after all.

A virtual machine is also more closed from the host's side. Even the root 
needs tricks to see what's happening inside. Not with LXC, the full process
tree is visible from the host's side, the files readable, processes are 
straceable etc. You can run stuff but you can't hide it. 
   
# Preparations

This is my own home system. It has a fairly complicated network setup, so what you 
will see here is not the simplest way to have networking. On the other hand, a real
usage scenario will look a lot more like this than a simple demo setup, so i 
keep this here. 

## The network

I wanted to use the containers as a dmz, and i have a shorewall setup, 
so i created a "blank" bridge:

    auto lxbr0
    iface lxbr0 inet static
        pre-up echo 1 > /proc/sys/net/ipv4/conf/all/proxy_arp #this caused me 2 hours of suck.
        address 192.168.4.1
        netmask 255.255.255.0
        bridge_ports none

I also added a new range to my dnsmasq setup:

    dhcp-range=192.168.4.5,192.168.4.45,12h

And finally a few bits to my shorewall config:

    /etc/shorewall/zones:
        dmz ipv4
    /etc/shorewall/interfaces:
        dmz lxbr0 detect dhcp   #the option dhcp enables various netfilter tricks to let through dhcp
    /etc/shorewall/policy:
        dmz all REJECT          #safe
    /etc/shorewall/rules:
        ACCEPT fw dmz tcp 22    #so we can ssh in
        ACCEPT dmz fw udp 53    #so dns works
        ACCEPT dmz net tcp 80   #so apt works
        ACCEPT loc dmz tcp 22 #ssh into the containers
        #these are neecessary for testing
        ACCEPT dmz loc icmp
        ACCEPT dmz fw icmp
        ACCEPT dmz net icmp
        ACCEPT loc dmz icmp     
        ACCEPT fw dmz icmp
        
## Installation

    apt install lxc

## Disk space

    /dev/mapper/ssd-lxc on /var/lib/lxc type ext4 (rw,relatime,discard,data=ordered)

Ext4, because why not. I decided to keep the containers on a separate partition,
using the `dir` backing fs, which means it has the directory structure there. 
There is no solution to limit the disk usage, so if i have more than one container
on this partition, those can starve each other.

## Default configuration

The file `/etc/lxc/default.conf` contains the configuration details for every new machine.

    lxc.network.type = veth                #virtual ethernet, connected to a bridge
    lxc.network.link = lxbr0                #this is the bridge
    lxc.network.flags = up                  #yes, we want it up
    lxc.network.hwaddr = de:ad:be:ef:xx:xx  #'x'es are replaced by random numbers
    lxc.arch = amd64                        #this is not necessary, it defaults to the host arch
    lxc.ephemeral = 1                       #the container disappears after stopping it.
    lxc.start.auto = 1                      #yes, it has not much sense here, but!

There can be other goodies in this file. If someone is without a dhcp server, 
an ip range can be set, and `lxc-create` chooses an ip randomly (or sequentially?).
`ephemeral` and `start auto` are an interesting combination, and has no sense here.
But there are ways to create some hierarchical configuration files, with tags marking
the various groups, and those can overwrite the ephemeral setting.

This is what you have to add to imitate a dhcp service:

    lxc.network.ipv4 = 192.168.4.0/24 255.255.255.0     
    lxc.network.ipv4.gateway = 192.168.4.1 

I don't really know how the ip address gets to the container in this case.

# A privileged container

This looked easier and simpler.

    # lxc-create --name sysdtest --template debian -- --enable-non-free --release=jessie --packages=sysvinit-core,sysvinit-utils

`lxc-create` does a few things for us. First, it creates a directory for the container. 
Based on the selected container, it creates that too, the default is a subdirectory 
called `rootfs`. Finally it creates a configfile, called `config`. It contains 
variables derived from the default configuration, `/etc/lxc/default.conf`. The
MAC and ip is selected from the defined ranges, if we gave any ranges.

    # lxc-info -n sysdtest
    Name:           db
    State:          STOPPED
    # lxc-start -n db
    root@melak:~# lxc-info -n sysdtest
    Name:           sysdtest
    State:          RUNNING
    PID:            8309
    IP:             192.168.4.6
    CPU use:        0.39 seconds
    BlkIO use:      420.00 KiB
    Memory use:     4.09 MiB
    KMem use:       1.31 MiB
    Link:           vethAVRR81
     TX bytes:      1.68 KiB
     RX bytes:      2.06 KiB
     Total bytes:   3.75 KiB
    # lxc-attach -n sysdtest
    root@sysdtest:~# w ; ps axf
     10:54:54 up 9 min,  0 users,  load average: 0.02, 0.16, 0.22
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
      PID TTY      STAT   TIME COMMAND
      666 ?        Ss     0:00 /bin/bash
      668 ?        R+     0:00  \_ ps axf
        1 ?        Ss     0:00 init [3]
      575 ?        Ss     0:00 /sbin/dhclient -4 -v [..] eth0
      652 ?        Ss     0:00 /usr/sbin/sshd
      660 console  Ss+    0:00 /sbin/getty 38400 console

Is it not it beautiful? With the dhcp/firewall/defaultconfig it has an ip address, 
we can ping it, we can ssh into it, should we knew the root password. 
But we dont. That's another story, for tomorrow.

# Detour: systemd fail.

First, the workaround, build your container with the systemv packages forced on it:

    lxc-create -n sysdtest  --template debian -- --enable-non-free --release=sid  --packages=sysvinit-core,sysvinit-utils

Now that this is out of the way, the problem: systemd is unable to start on an 
lxc container as it is working on a debian stretch host. The reason, according to 
the debug gracefully shared by systemd is:

    root@sysdtest:~# /lib/systemd/systemd --system
    systemd 232 running in system mode. (+PAM [..] +IDN)
    Detected virtualization lxc.
    Failed to write /run/systemd/container, ignoring: No such file or directory
    Detected architecture x86-64.
    Set hostname to <sysdtest>.
    Cannot determine cgroup we are running in: No data available
    Failed to allocate manager object: No data available

I tried to follow 
[this description](https://developers.redhat.com/blog/2016/09/13/running-systemd-in-a-non-privileged-container/),
but whatever configuration of cgroup mounts i tried, systemd was not 
obliging. I gave it up. I'm not a fan of systemd anyways.

# Unprivileged container

This is the preferred one. Common user running a container, no root involved!

First we need our subuid/subgid, which is a cgroup magic and needed to make us 
root inside the container and a noone outside.

    # grep geza  /etc/subgid /etc/subuid
    /etc/subgid:geza:231072:65536
    /etc/subuid:geza:231072:65536

Allow the user to have network access, without that life would be lonely. 
The file `/etc/lxc/lxc-usernet` needs this line:

    geza veth lxbr0 5

The user `geza` can create virtual ethernet interfaces attached to `lxbr0`,
five instances.

Then edit the `.config/lxc/default.conf` file. Other than the ids i'm including
the master configuration to have network.

    lxc.include = /etc/lxc/default.conf
    lxc.id_map = u 0 231072 65536
    lxc.id_map = g 0 231072 65536
    lxc.ephemeral = 0

For some complicated reason i don't understand the `debian` build template
does not works for unprivileged containers, we have to use `download`. 
Therefore we can not use the nice workaround for initd. The os/relase/arch can
be selected without the options at the end, it just looks better this way.

    geza@melak:~$ lxc-create -n doboz -t download -- -d debian -r sid -a amd64

Again a systemd-equipped system which is unable to run. And now we have to fix
it manually:

    geza@melak:~$ lxc-attach -n doboz
    root@doboz:/# ps axf
      PID TTY      STAT   TIME COMMAND
       23 ?        Ss     0:00 /bin/bash
       24 ?        R+     0:00  \_ ps axf
        1 ?        Ss     0:00 /lib/systemd/systemd
    root@doboz:/# /sbin/dhclient eth0
    root@doboz:/# echo "nameserver 192.168.4.1" > /etc/resolv.conf
    root@doboz:/# export PATH=$PATH:/sbin:/usr/sbin
    root@doboz:/# apt install sysvinit-core sysvinit-utils1:
    Reading package lists... Done
    Building dependency tree       
    Reading state information... Done
    [...]
    sysvinit: restarting...init: timeout opening/writing control channel /run/initctl
    .init: timeout opening/writing control channel /run/initctl
    [repeats several times]
    .init: timeout opening/writing control channel /run/initctl
     failed.
    Processing triggers for systemd (232-22) ...
    root@doboz:/# cp /usr/share/sysvinit/inittab /etc/inittab
    root@doboz:/# exit
    exit
    geza@melak:~$ lxc-stop -kn doboz
    geza@melak:~$ lxc-start -n doboz
    geza@melak:~$ lxc-attach -n doboz
    root@doboz:/# ps axf
      PID TTY      STAT   TIME COMMAND
      653 ?        Ss     0:00 /bin/bash
      654 ?        R+     0:00  \_ ps axf
        1 ?        Ss+    0:00 init [2]
      574 ?        Ss     0:00 /sbin/dhclient -4 -v -pf /run/dhclient.eth0.pid -lf /var/lib/dhcp/dhclient.eth0.leases -I -df /var/lib/dhcp/dhclient6.eth0.leases eth0
      647 pts/0    Ss+    0:00 /sbin/getty 38400 tty1

Please be aware to the `-kn` at the `lxc-stop` command. Without a working init
lxc cannot halt the container properly, it has to kill it.

Now, how could we make this manual step out of the way? Other than using 
a distribution which has no systemd. 

## Automated creation via hooks

The LXC system provides some hooks, scripts which we can execute at various points 
in the boot process. To demonstrate this, i'll present a hackish way to make
the create/build process fully automated. I will use two hooks:

    lxc.hook.pre-start = ~/fix.sh
    lxc.hook.start = /var/tmp/fixinit.sh

The `lxc.hook.pre-start` is executed before the system boots up, and it is 
running on the host. Fortunately LXC provides variables to know where we find 
things. The script:

    #!/bin/sh 
    cp ~/fixinit.sh /"$LXC_ROOTFS_PATH"/var/tmp/
    exit 0

And the `lxc.hook.start` runs on the host, but before `init`, which is lucky,
as we want to fix the init itself. The script:

    #!/bin/sh
    if [ -L /sbin/init ] ; then
        export PATH=$PATH:/sbin:/usr/sbin
        dhclient eth0
        apt-get -qy  install sysvinit-core sysvinit-utils openssh-server 
        mkdir /root/.ssh
        echo "ssh-rsa AA[..]ph tudor@melak" > /root/.ssh/authorized_keys
        chmod 700 /root/.ssh
        chmod 600 /root/.ssh/authorized_keys
        sed 's+/etc/init.d/powerfail start+/sbin/poweroff+' < /usr/share/sysvinit/inittab > /etc/inittab
    else
        echo "All fixed already."
    fi

The script does a few other things: adds an ssh key and an ssh server, so 
a potentially existing configuration management tool can log in, and changes
the `/etc/inittab` file a bit, to make `lxc-stop` work. The `/etc/init.d/powerfail`
script was not there after the package installs and i really don't need it.

The first start last a few minutes, because of the init timeout. Sad.

## An Alpine linux mysql container

Because systemd is evil, and [Alpine Linux](https://alpinelinux.org/) is supposed to be a container friendly, minimal linuxlet. 
Steps:

1. Create an `sql` user, because there will be more than one sql container in the future, find the subuid and subgid, as above.
1. Create and `sql` directory for the rootfses:

        mkdir /var/lib/lxc/sql
        chown sql:sql /var/lib/lxc/sql

1. Create the container itself, special note of the `--dir` option, my `home` is mounted as `noexec`, and trying to start a container on that would result in a permission denied for `/sbin/init`.

        lxc-create -n mysql -t download --dir /var/lib/lxc/sql -- -d alpine -r 3.8 -a amd64

1. Attach, and follow [the description helpfully provided by the Alpine Linux Team](https://wiki.alpinelinux.org/wiki/MariaDB)

        lxc-attach -n mysql
        apk add mariadb mariadb-client
        [etc]
        
1. Shorewall, only enabling from the firewall as thats where i'm trying to use it, edit ` /etc/shorewall/rules`:

        #mysql from the fw
        ACCEPT fw dmz tcp 3306

    Never forget to reload your firewall configuration:

        /etc/init.d/shorewall reload

1. Add a user to run statistics:

        MariaDB [(none)]> CREATE user 'ubul' IDENTIFIED BY 'zeno';
        Query OK, 0 rows affected (0.00 sec)

1. Check if this works from the server you want to run munin:

        # mysqladmin -h 192.168.4.11 -u ubul -pzeno status
        Uptime: 1761  Threads: 9  Questions: 11  Slow queries: 0  Opens: 17  Flush tables: 1  Open tables: 11  Queries per second avg: 0.006

    It's actually works. This is somewhat scary.    

1. Set up Munin, because without monitoring things are not real:

        ln -s /usr/share/munin/plugins/mysql_queries /etc/munin/plugins

    Edit `/etc/munin/plugin-conf.d/munin-node`:

        [mysql*]
        user root
        env.mysqlopts -h 192.168.4.11 -u ubul -pzeno 

    And of course restart and test:

        # /etc/init.d/munin-node restart
        # munin-run mysql_queries
        delete.value 0
        insert.value 0
        replace.value 0
        select.value 7
        update.value 0
        cache_hits.value 0

1. add user to mysql, add grants.

1
        

        

# Mounting a host directory

I have not tried this on a privileged container, and on an unprivileged one it 
has some ugliness. First of course we need a mounted filesystem on the host: 

    root@melak:/var/holmi/cucc# df -h .
    Filesystem             Size  Used Avail Use% Mounted on
    /dev/mapper/big-holmi  5.0G   33M  5.0G   1% /var/holmi
    root@melak:/var/holmi/cucc# touch ezacucc

Then we need a line into the configuration of the new container. It's stored
in the directory `~/.local/share/lxc/<name>/`. 

    lxc.mount.entry = /var/holmi/cucc mnt none bind 0 0

Looks like an fstab line, but thats a trick! The first one is the directory on
the host, the second one is the mountpoint relative to the root of the container.
Obviously this must be created first.

Restart the container, and see hot this looks like:

    root@doboz4:/mnt# ls -l
    total 0
    -rw-r--r-- 1 nobody nogroup 0 May  6 17:36 ezacucc
    root@doboz4:/mnt# df -h .
    Filesystem             Size  Used Avail Use% Mounted on
    /dev/mapper/big-holmi  5.0G   33M  5.0G   1% /mnt
    root@doboz4:/mnt# touch igazan
    touch: cannot touch 'igazan': Permission denied

Rampant information leakage. And the uid/gid on the host means nothing in the 
container. 

    root@melak:~# ls -ld /home/geza/.local/share/lxc/doboz4/rootfs/
    drwxr-xr-x 22 231072 231072 239 May  5 01:05 /home/geza/.local/share/lxc/doboz4/rootfs/

This is our secret number. One can see this in the config too.

    root@melak:~# chown 231072:231072 /var/holmi/cucc/

And:

    root@doboz4:/mnt# touch igazan
    root@doboz4:/mnt# ls -l
    total 0
    -rw-r--r-- 1 nobody nogroup 0 May  6 17:36 ezacucc
    -rw-r--r-- 1 root   root    0 May  6 17:46 igazan

And from this moment this directory is ours:

    root@doboz4:/mnt# adduser test
    root@doboz4:/mnt# mkdir testdir
    root@doboz4:/mnt# chown test:test testdir/
    root@doboz4:/mnt# ls -l
    total 0
    -rw-r--r-- 1 nobody nogroup 0 May  6 17:36 ezacucc
    -rw-r--r-- 1 root   root    0 May  6 17:46 igazan
    drwxr-xr-x 2 test   test    6 May  6 17:48 testdir
    tudor@melak:~$ ssh test@doboz4
    test@doboz4:~$ cd /mnt/testdir/
    test@doboz4:/mnt/testdir$ touch booo
    test@doboz4:/mnt/testdir$ ls -l
    total 0
    -rw-r--r-- 1 test test 0 May  6 17:49 booo
    test@doboz4:/mnt/testdir$ 

This creates some strange uid/gids on the host side:

    root@melak:/var/holmi/cucc# ls -l testdir/booo 
    -rw-r--r-- 1 232072 232072 0 May  6 19:49 testdir/booo

There is a chance that if someone has a lot of containers these numbers can 
collide, in which case two random users can have the same userid and on a 
shared directory they could write each other's files. I dont know if there 
is a mechanism to stop that, but i will sleep soundly if this is the largest 
problem.

# Monitoring and logging.

First, it seems that there is a way to run secretly containers. The command 
`lxc-ls`, which is supposed to list those needs a `-P` option to find the
configured containers. IF the sysadmin does not knows that i have containers in
my home what she/he can do?

    root@melak:~# lxc-ls
    sysdtest 
    torr@melak:~$ lxc-ls 
    titkos 

There are various proc and sysfs paths which could help, but i'm a lazy man so
i'm inclined to use this quiet little horror:

    ps ax | 
    awk '/\[lxc monitor\]/ {print $(NF-1),$NF}' | 
    while read path name ; do lxc-info -H -P $path -n $name ; done

I don't know how to monitor cpu usage yet. The cgroups could help with that. Maybe. Hopefully. 

As for logging, it's very simple:

    lxc.loglevel = 1
    lxc.logfile = /home/geza/doboz.log

Level 1, debug, is quite chatty. 

# Information morsels

1. Almost everything interesting is in the 
   [manual of the configuration](https://linuxcontainers.org/lxc/manpages/man5/lxc.container.conf.5.html).
   It has all the configuration options and hooks and variables.
1. Networking is not working without enabling proxyarp.
1. Systemd is not working and i cannot make it. There is a workaround (for debian).
1. The images created by lxc are very limited, cron, mta, man, a lot of useful 
   packages are missing. This makes sense if we want to use the machines as lightweight
   toys. My recommendation is `dma` for mta, its smaller than exim4. 
1. LXC maintains a debian package cache/proxy at the directory 
   `/var/cache/lxc/debian/`. I can imagine a similar cache for other linux 
   flavors. This means that only the first lxc-create needs a lot of waiting, 
   afterwards it's just a few seconds to start a new one. 
1. `lxc-stop` is not working if the `init` in the container is not working or not
   handles kill signals as almost every init tool does. The actual signal is
   configurable, if someone has the brains and perseverance to make systemd run.
   Until that happens, use `lxc-stop -k -n <name>` which will kill the 
   processes.
1. A container's rootfs directory can not live on a partition which mounted 
   with the `noexec` option. `nodev` and `nosuid` is also suspected.
