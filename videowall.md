# Attempts to make a videowall

A videowall would be the composite image of more than one cameras in one picture, like the screens watched by security personnel. 
I am planning to use my laptops webcam as a source and my tv as a screen.

## Streaming

### Streaming attempt 1: ffmpeg

1. copy the `ffserver.conf' file to the laptop
1. start it with `ffserver -f ffserver.conf`
1. start a recorder with `fmpeg -v 2 -r 10 -s 1280x1024 -f video4linux2 -i /dev/video0 http://127.0.0.1:8090/webcam.ffm`

This works, eats some cpu.

### Streaming attempt 2: mjpeg-stream

This tool was designed for webcams, it has a nice builtin webserver, and was not updated since 2014. On the other hand, it was forked for the raspberry pi and this fork is relatively maintained. I had to compile it, as it's not in debian, sadly.

    sudo apt-get install cmake libjpeg8-dev
    unzip ../Letöltések/mjpg-streamer-master.zip 
    cd mjpg-streamer-master/mjpg-streamer-experimental/
    make
    ./mjpg_streamer -i "input_uvc.so -f 10 -q 95 -r 1280x1024" -o "output_http.so -w www/ -l 0.0.0.0 -n"
    
    mpv --autofit 704x528 --geometry=720:544  --no-border --cache=no http://toothless:8080/?action=stream

It uses much less cpu but much more bandwidth. The ffmpeg solution eats 180kbyte/s, this one eats 2 Mbyte/s. It's latency also better and it has no compression artifacts. I think it would work for wired laptops.

### Streaming attempt 3: nginx-rtmp

The helpful folks at #ffmpeg told me that the ffserver is badly outdated, and this is why there are random strange occurrences, like not adding the datetime. They recommended the rtmp plugin for nginx.

Alas, this does not exists in Debian, so I had to roll my own nginx-full package. I followed this description:

https://wiki.debian.org/DhyanNataraj/RtmpVideoStreamingViaNginx

The only major difference is that the `debuild` command I used was this:

    debuild -d -i -us -uc -b

Thus I can avoid complaints because of the missing systemd dependencies and the inability to sign the packages.

It actually needed much more packages, but at the first run `debuild` properly notified me. Good boy.

The configuration itself was fairly simple, appended the below at the end of `/etc/nginx/nginx.conf`:

    rtmp {
        server {
            listen 1935;
            application live {
                live on;
            }
        }
    }

This is more than enough to stream stuff.

And to stream to this, here is a full-blown command line.

    ffmpeg -re -s 1280x1024 -framerate 10 -f video4linux2 -i /dev/video0 -vf drawtext="15:y=15:fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf:text='%{localtime\:%F %H.%M.%S}'-$HOSTNAME:fontcolor=white@0.85:fontsize=30:box=1:boxcolor=black@0.5:boxborderw=5" -c:v libx264 -qscale:v 2 -f flv  rtmp://gw/live/"$HOSTNAME"

Uhh. Let's make it multiline, it's easier that way:

    ffmpeg 
        -re -s 1280x1024 -framerate 10 -f video4linux2 -i /dev/video0 
        -vf drawtext="
            x=15:
            y=15:
            fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf:
            text='%{localtime\:%F %H.%M.%S}'-$HOSTNAME:
            fontcolor=white@0.85:
            fontsize=30:
            box=1:
            boxcolor=black@0.5:
            boxborderw=5" 
        -c:v libx264 
        -qscale:v 2 
        -g 10
        -f flv  
        rtmp://gw/live/"$HOSTNAME"

So far the only interesting item is the option `-c:v`, which defines the encoding used in the flv container. `libx264` creates less bandwidth, but uses much more cpu, so much that a Thinkpad x301 has trouble keeping it up. Leaving it out results in `flv`, which results in 3 mbyte/s bandwidth and a happy cpu.

Using `libx264` also causes more delays, `flv` has around 9-10 seconds, `libx264` has 13-14. Obviously decoding this codec needs more power from the videowall server too.

Adding the `-g 10` option makes the so called "keyframes" to happen more often. As the initial delay caused by waiting for the first keyframe this makes the overal liveliness of the pictures better.

### Streaming attempt 4: the pis

By a strange coincidence I have two raspberry pis with cameras around. As these are in weatherproof boxes it would be cute to use them to watch the surroundings, making me almost blofeldianly evil. The only problem is that their tiny little cpu is unable to handle the h264 codec, and as they're on wifi, the bandwidth is not enough for mjpeg or flv.

But these are mobile cpus, so they have multimedia accelerators, so they can generate h264 cheaply! Thus ffmpeg only had to rewrap the video and to push it to my rtmp server.

I got my ffmpeg from this repo: 

http://packages.hedgerows.org.uk/

The Raspbian distribution has only libav, which does not seems to work too well.

And the command line to do the magic:

    raspivid -t 0 -pf high -a 1037 -a "%F %T-$HOSTNAME" -ae 30 -o - | ffmpeg -loglevel panic -re -i - -c:v copy -g 10 -f flv rtmp://gw/live/"$HOSTNAME"

This puts out a stream of 500-1000 kbyte/sec bandwidth, full hd, usable quality, wit the cpulet load around 40-50%.

## Walling

### Walling attempt 1: ffmpeg

ffmpeg can do some wild magic, but it's not designed for display. After some googling i put this together:

    ffmpeg \
        -re -i http://fifi:8090/webcam.flv \
        -re -i http://luigi:8090/webcam.flv \
        -filter_complex "
            nullsrc=size=2560x1024 [base]; 
            [0:v] copy [left]; 
            [1:v] copy [right];
            [base][left] overlay [tmp1];
            [tmp1][right] overlay=x=1280
        "\
        -f mpegts - | 
    mpv -quiet -fs -

It works, it creates a nice picture, but after a few minutes the inputs start to skip, first for seconds, later for minutes, then until my patience runs out.

I was trying to add a date/time/hostname to the picture. This works:
    
    ffplay  -s 1280x1024 -f video4linux2 -i /dev/video0 -vf drawtext="15:y=15:fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf:text='%{localtime\:%F %H.%M.%S}'-$HOSTNAME:fontcolor=white@0.85:fontsize=30:box=1:boxcolor=black@0.5:boxborderw=5"

This does not works:

    ffmpeg -v 2 -r 10 -s 1280x1024 -f video4linux2 -i /dev/video0 -vf drawtext="x=15:y=15:fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf:text='%{localtime\:%F %H.%M.%S}'-$HOSTNAME:fontcolor=white@0.85:fontsize=30:box=1:boxcolor=black@0.5:boxborderw=5" http://127.0.0.1:8090/webcam.ffm

Tanakodás is on.

### Walling attempt 2: mpv geometry

    mpv --autofit 960x540 --no-border --cache=no --geometry=0:0 http://fifi:8090/webcam.flv 

This works pretty well. This means of course that N mplayers will run in parallel, but the cpu can take it. With the `--geometry` option one can pack the windows nicely.

### Walling refinement 1: fixed mpv and remote control

Start mpv thus:

    mpv --autofit 704x528 --geometry=720:544  --no-border --cache=no \
    --idle=yes --keep-open=always --force-window=immediate --force-window-position \
    --input-ipc-server=/tmp/mpvsocket

The second line of arguments are all needed to keep the window exactly where it should be, the third opens a socket which can be used to issue commands in json (sorry):

    $ echo '{ "command": ["loadfile", "http://toothless:8080/?action=stream", "replace"] }'  | socat - /tmp/mpvsocket
    {"data":null,"error":"success"}

### Walling refinement 2: fixed mpv and movable windows

Obviously the ideal end state is when i have all the streams on the screen in a tiny window and can change one to be big so i can see the activity better. There are two trivial solutions for this:

   1. Having N small and one large prepared mpv windows and add the interesting stream to the large one. This would mean double the bandwidth. Bad.
   1. Moving the windows around. Mpv is evil and does not let's me do it, and xlsclients is not working if you are not on a real desktop, but `wmctrl` does!

To use wmctrl comfortably, mpv needs the `--title windowname` option, so `wmctrl` can see and use proper names, not bits of the filename, to which mpv changes the title automagically. Then:

    $ wmctrl -l
    0x00800002  0 N/A m1
    0x00a00002  0 N/A m4
    0x00600002  0 N/A m2
    0x00c00002  0 N/A m3

And when this is done I can move the windows at will!

    $ wmctrl -r m1 -e 0,334,3,1280,1024
    $ wmctrl -r m1 -e 0,3,3,330,264

All I need now is a little script to do the number crunching and mpv starting/stopping for me.

## Other stuff

### Little scripts!

Today I wrote a quite ugly python script, called `videowall.py`. So far i'm unable to set the `DISPLAY` variable from the command line for `mpv`, so before starting it it needs an `export DISPLAY=:0.0`. Maybe later i'll check how to set environment variables for Popen?

The only argument it accepts is ether a stream or a filename with streams in it. Its behaviour is not well defined to too many streams. It does not checks if the argument is a stream because, seriously, mpv can play any wild string. After that it asks for input in a prompt. 

   * 'q' will make it quit, 
   * 'a streamurl' adds a new stream
   * 'r streamid' restarts the stream
   * 'p streamid' toggles pause (necessary after a restart sometimes)
   * a number will make that stream big

To query the maximum resolution for the laptop's camera:

    lsusb | 
    awk '{print $6}' | 
    sort -u | 
    while read vendid ; do 
        lsusb -vd $vendid | 
        awk '/wWidth/ {w=$2} /wHeight/ {print $2*w,w,$2}' 
    done | 
    sort -n | 
    tail -1 |
    cut -d " " -f 2,3

### Archiving 

These files are quite large. The flv files are 850 MB/minute. It would be nice if i could compress them to h264, thus sparing some disk space, as the compressed file uses half as many disk space. Unfortunately, my old weak little cpu is barely able to compress one file at real time speed, and I have three cameras (laptops) generating flv format files.

When I keep only the keyframes, the resulting video (of course) is totally unusable, but at least I can store something. Also, apparently further compression of these files are not useful - but i will check that.

The configuration needed for this archiving:

    rtmp_auto_push on;
    rtmp {
        server {
            listen 1935;
            application live {
                live on;
                play_restart on;
                record keyframes;
                record_path /mnt;
                record_suffix -%F-%T.flv;
                record_lock on;
                record_max_size 1000M;
                record_interval 1h;
            }
        }
    }

The lock is to determine which files are still used if one wants to compress/write to tape the ready ones. With recording keyframes 1GB seems to be a good limit, the flv spewing smaller machines do 850 MB per hour. The h264 ones do around 200-250 MB per hour, obviously depending on the light conditions. 

