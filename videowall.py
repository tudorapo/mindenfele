#!/usr/bin/python
"""
A script to display several video streams on one screen, with the possibility 
to switch between them.

You know, that thing the security guards never watch in the Bond movies.

Use: videowall.py stream stream file_with_one_stream_per_line stream ...

This is epxected to be run on a remote server with a screen (mediaserver) 
because it waits for input on the terminal.

After a few seconds the videos are coming up and a promp appears. There are 
a few commands to use. The commands are all lowercase and only the first 
character checked.

    "quit"  - quit.
    "pause STREAMNUM" - pause or unpause a stream. When a stream stops for 
        any reason (ffmpeg crashes, Bond shoots down a camera) mpv pauses, 
        and after reload this must be issued.
    "reload STREAMNUM" - reload a stream. If its a movie it starts from the
        beginning, if its a restarted stream, see pause.
    "add STREAM" - adds a new stream. 
    "STREAMID" - puts this stream onto the big screen.

There is no way to delete a stream, restart the whole stuff. There is no good
way to deal with the unused mpv subprocesses. I should write them as objects.

"STREAMID" is a number, from 0 to the number of streams minus one. 

"STREAM" can be anything wich can be played by mpv. To see my experience
with streaming laptop cameras:

https://gitlab.com/tudorapo/mindenfele/blob/master/videowall.md

Should you have any comments or suggestions, don't be afraid to contact me.

License, dunno, CC0 looks okay?

"""

import subprocess
import socket
import sys
import time
import json
import os

def rearrange(num, look, env, ugly):
    #rearranging windows
    n = 0
    for i in range(num):
        w='vv'+str(i)
        c=['/usr/bin/wmctrl','-r',w,'-e']
        if i == look :
            g=[0,ugly["x"]+(2*ugly["b"]),ugly["b"],ugly["bx"],ugly["by"]]
        else:
            g=[0,ugly["b"],(n*(ugly["y"]+ugly["b"]))+ugly["b"],ugly["x"],ugly["y"]]
            n=n+1
        c.append(','.join(map(str, g)))
        subprocess.call(c,env=env)

def uglymath(num,sx,sy,vx,vy,border):
    #ugly maths
    unit=4
    try:
        t=((sy-(num*border))/(num-1))/unit
    except ZeroDivisionError:
        print("I need at least two streams.")
        exit(-2)

    x=int(t*va*unit)
    y=int(t*unit)

    if x>sx-(vx+(3*border)):
        x=sx-(vx+(3*border))
        y=int(x/va)

    bx=sx-(x+(3*border))
    by=sy-(2*border)
    print x,y,y*num,va
    ugly={"x":x,"y":y,"b":border,"vx":vx,"vy":vy, "bx":bx, "by":by}
    return ugly

def loadstream(stream,num):
    j={'command':['loadfile',stream,'replace']}
    zokni=socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    zokni.connect(fifo + str(num))
    zokni.sendall(json.dumps(j))
    zokni.sendall("\n")
    zokni.recv(120)
    zokni.sendall
    zokni.close()

def pokestream(num):
    j={'command':['keypress','p']}
    zokni=socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    zokni.connect(fifo + str(num))
    zokni.sendall(json.dumps(j))
    zokni.sendall("\n")
    zokni.recv(120)
    zokni.sendall
    zokni.close()

def startmpv(num,fifo,env):
    command="/usr/bin/mpv --title vv" +\
        str(num) + " --geometry=4:" +\
        str(4+(num*204)) + " --input-ipc-server=" +\
        fifo + str(num) + mpvargs
    c=command.split()
    mpv=subprocess.Popen(c,env=env)
    return mpv

def killmpv(mpvs):
    #killing mpvs
    for mpv in mpvs:
        mpv.terminate()
        mpv.wait()

if len(sys.argv) < 2:
    print("I need at least two streams to work with.")
    exit(-1)

#standard mpv args
mpvargs=" --no-border --no-keepaspect-window --cache=no --idle=yes --keep-open=always --force-window=immediate --no-terminal "

#fifo places
fifo="/tmp/mpvsmall"
display=":0.0"

myenv={"DISPLAY": display}

#screen dimension
sx=1920
sy=1080
border=10

#video dimension
vx=1280
vy=1024
va=float(vx)/float(vy)

#arrays to put stuff in it
mpvs=[]
streams=[]

#remove the command
print(sys.argv.pop(0))

#read in args and files
for arg in sys.argv:
    if os.path.isfile(arg):
        with open(arg, "r") as streamf:
            for stream in streamf:
                streams.append(stream.rstrip('\n'))
    else:
        streams.append(arg)

print streams

matek=uglymath(len(streams),sx,sy,vx,vy,border)
print matek

#starting mpvs
for i in range(len(streams)):
    mpvs.append(startmpv(i,fifo,myenv))

#adding streams
time.sleep(2)
for i in range(len(streams)):
    loadstream(streams[i],i)
print "Waiting for streams to normalize."
time.sleep(10)

look=0
while True:
    rearrange(len(streams),look,myenv,matek)
    key=raw_input("Mongy valamit:")
    if key.startswith('q') :
        break
    elif key.startswith('a'):
        stream=key.split()[1]
        print "Adding new stream ",stream
        streams.append(stream)
        matek=uglymath(len(streams),sx,sy,vx,vy,border)
        mpvs.append(startmpv(len(streams)-1,fifo,myenv))
        time.sleep(2)
        loadstream(stream,len(streams)-1)
    elif key.startswith('r') or key.startswith('p'):
        try:
            i=int(key.split()[1])
        except (ValueError, IndexError):
            print "I need a number here."
            continue
        if i >= len(streams):
            i=len(streams)-1
        if i < 0:
            i=0
        print "Reloading stream ",i
        if key.startswith('r') :
            loadstream(streams[i],i)
        if key.startswith('p'):
            pokestream(i)
    try:
        look=int(key)
    except ValueError:
        pass
    if look >= len(streams):
        look=len(streams)-1
    if look < 0:
        look=0
    print "Command:",key,"\nCurrent stream:",look

killmpv(mpvs)

