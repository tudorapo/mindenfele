# Laptop comfortability

When a new laptop arrives into a family it's always a hustle to make it 
practical. I always try to use something pre-comforted, but then i always give 
it up because it has so many more comfortability than I need. I press random 
keys all the time, because i am clumsy, which results in various
misconfigurations and bad surprises.

Therefore after a while I go back to openbox and alsa and other simple things.
But then I have to set up the Secret Keys to do my work. For the first time in
my life I will document this. Revolution.

## Basic principles

The system runs Devuan Ascii and openbox. I try to make every keys handled by 
Openbox, but this fails now because the mic mute key is claimed by acpi. I could
set it with various Magic Words sent to the `thinkpad_acpi` module, but I don't.

I learned the keycodes by using `xev`.

I need screen brightness, volume, mute, mic mute, screenshot, lock, sleep.

## Screen brightness

This week it can be set by echoing integers into 
`/sys/class/backlight/intel_backlight/brightness`, and the largest integer can 
be seen from `/sys/class/backlight/intel_backlight/max_brightness`. 

Write an ugly script to change this number by ten percent up and down, and use 
`XF86MonBrightnessUp` and `XF86MonBrightnessDown` in your openbox rc.xml.

To let a common user access this file to write, you have to add the below two 
lines to `/etc/udev/rules.d/80-backlight.rules`:

```
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"
```

## Mute key

No, I don't know why, but this is managed by acpi now. So, first learn what key
is sent when you press it by `acpi_listen`. Then add the following to
`/etc/acpi/events/lenovo-mutemic`:
```
event=button/micmute MICMUTE 00000080 00000000 K
action=/usr/bin/amixer sset 'Capture',0 toggle

```

And this evne switches the led on it! Well done.

## Sleep

This is where we go hardcore ugly, because instead of doing the rules.d dance,
or doing something with setuid, I did it with sudo. I'm really sorry.

Put the below into a script:
```
echo mem > /sys/power/state
```
Link the scrupt to `/usr/local/bin/` because why not, and allow your user to 
execute everything as `ALL=NOPASSWD: ALL`. Voilá.

Find a good looking key, I'm using `XF86Tools`.

## Everything else

The other audio stuff is simple, use keys like `XF86AudioMute` and commands like
`amixer sset Master playback 1%-`. 

Screen lock is also, use `xscreensaver-command -lock` and `W-l`, which is 
Microsoft+l so a windows user will know at least one key.

## Other behaviour

In the `autostart` file of Openbox put the following, to avoid unexpected
actions:
```
xset -dpms
xset s off
xset b off
```
Set your background, keys, conky as you feel.