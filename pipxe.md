# Network booting a Raspberry Pi4

## Executive Summary

You need an experimental/beta firmware, which means some bravery. 
I have an established pxe environment at home, so my pxe shenanigans 
will be different from yours. 

## The firmware

[Official beta writeup](https://github.com/raspberrypi/rpi-eeprom/blob/master/firmware/raspberry_pi4_network_boot_beta.md)

Note the tftp prefix. I never touched it, but after a while the tftp requests 
started to arrive with that path. I just shrugged and went with it.

Setting the `NET_BOOT_MAX_RETRIES` parameter to -1 helps with debugging.

Setting the `TFTP_TIMEOUT` parameter to a larger value may helps with the 
significant amount of timed out tftp requests.

## PXE

Oh my, this was fun. So I have semicomplicated PXE setup at thome, your 
trials could be worse or better.

* `dhcp-host=dc:a6:ha:ha:hi:hi,bigmac,192.168.A.B,set:pipxe` - add your host
    to the hostlist. Note the tag to be used in configs.
* `dhcp-option=66,192.168.2.1` - This one is sent by default, but not as
    option 66, but the pi asked for it, so I sent it. Not checked yet if it
    works without it.
* `dhcp-boot=tag:pipxe,pipxe/bootcode.bin` - yepp it needs this. In a subdir,
    because it needs a lot of other tiny files, see below.
* `pxe-service=0, "Pi boot"` - another one I'm not sure I need, will check 
    later.

## The Files

[Official writeup without Pi4](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/net_tutorial.md)

There are differences because I was not doing it from another pi but from a 
proper host.

The basic procedure is:

* Download and unzip a raspbian installer image.
* Mount it's two halves for example using `kpartx`.
* Copy the boot partition's content to the tftp directory. In my case it
    was `$TFTPROOT/pipxe/192.168.A.B/`, in the off chance that I will pxe
    another machine. Then it started to ask for files under the directory
    `$TFTPROOT/pipxe/192.168.A.B/serial456/` so I copied everything under 
    that directory too.
* Copy the larger directory to the nfs root directory
* Set up the two NFS exports, one for the tftp directory, one for the nfsroot
    directory, using the options `(rw,sync,no_subtree_check,no_root_squash)`:
    ```
    /var/nfsroot/pipxe 192.168.A.0/24(rw,sync,no_subtree_check,no_root_squash)
    /var/tftp/192.168.A.B  192.168.A.0/24(rw,sync,no_subtree_check,no_root_squash)
    ```
* In the tftp/boot directory change the file `cmdline.txt` to have this:

    `console=serial0,115200 console=tty1 root=/dev/nfs nfsroot=192.168.A.1:/var/nfsroot/pipxe,vers=4 rw ip=dhcp rootwait elevator=deadline`

    Don't be misled by the `vers=4` part, it is ignored and the whole thing 
    goes through udp. 
* In the nfsroot directory change the file `fstab` to remove the sdcard mount:
    ```
    proc            /proc           proc    defaults          0       0
    192.168.A.1:/var/tftp/192.168.A.B /boot nfs defaults,vers=3 0 0
    ```
    Be aware that this is the tftp/boot directory, the root is done from the 
    command line
* In the tftp/boot directory touch the file `ssh` so the ssh server starts.
* Reboot, reconfigure firewall rules, tcpdump, wireshark, because there is no 
  error logging. Have fun in genera.

## Aftermath

When tried to do an upgrade, the kernel was not upgrading because it was 
trying to do something with the sdcard, and the logs were full with an MMC 
timeout.

The process to upgrade the kernel is TODO.

Also checking if all PXE configuration options are needed.
