#!/bin/bash
if [ "$1" = "power" ] ; then 
    export CODEC="libx264"
else
    export CODEC="flv"
fi
echo Streaming from: $HOSTNAME using codec $CODEC

ffmpeg \
    -loglevel error\
    -nostdin\
    -re -s 1280x1024 -framerate 10 -f video4linux2 -i /dev/video0\
    -vf drawtext="
        x=15:
        y=15:
        fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf:
        text='%{localtime\:%F %H.%M.%S}'-$HOSTNAME:
        fontcolor=white@0.85:
        fontsize=30:
        box=1:
        boxcolor=black@0.5:
        boxborderw=5" \
    -c:v "$CODEC"\
    -g 10\
    -qscale:v 1 \
    -f flv \
    rtmp://gw/live/"$HOSTNAME" 
